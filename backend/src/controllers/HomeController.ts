import { JsonController, Get, Res, HttpCode } from "routing-controllers";
import { Response } from "express";
import { OpenAPI } from "routing-controllers-openapi";

@JsonController("/")
export class HomeController {
    @HttpCode(200)
    @Get("")
    @OpenAPI({
        summary: "Welcome page",
        description: "This route serve standard 'Hello world' page",
        statusCode: "200",
    })
    public async welcome(@Res() res: Response) {
        return res.status(200).send({
            result: "Hello World",
        });
    }
}
