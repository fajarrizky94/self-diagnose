import { Container } from "typedi";
import { createConnection, useContainer } from "typeorm";

/**
 * Setup in memory database
 * @param entities
 */
export async function createMemoryDatabase() {
    useContainer(Container);
    return createConnection({
        type: "sqlite",
        database: ":memory:",
        entities: [__dirname + "/../../src/entities/*{.ts,.js}"],
        dropSchema: true,
        synchronize: true,
        logging: false,
    });
}
