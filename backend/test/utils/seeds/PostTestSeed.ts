export const PostSeed = [
    {
        id: "6d2deecf-a0f7-470f-b31f-ede0024efece",
        title: "Example post.",
        content: "Lorem ipsum dolor sit amet.",
        previewContent: "Lorem ipsum.",
        createdAt: `${new Date()}`,
        updatedAt: `${new Date()}`,
    },
    {
        id: "5d1deecf-a0f7-470f-b31f-ede0024efece",
        title: "Another example post.",
        content: "Lorem ipsum dolor sit amet.",
        previewContent: "Lorem ipsum.",
        createdAt: `${new Date()}`,
        updatedAt: `${new Date()}`,
    },
];
