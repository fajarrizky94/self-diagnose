import * as React from 'react';
import { Box } from 'components/ui-core';

const LayoutRoot: React.FC = ({ children }) => (
  <Box
    as="main"
    display="flex"
    flexDirection="column"
    position="relative"
    minHeight="100vh"
    overflowX="hidden"
    color="foreground"
    backgroundColor="background"
  >
    {children}
  </Box>
);

export default LayoutRoot;
