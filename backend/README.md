## ❯ Self-diagnose backend

Created using express + TS

### TechStack

-   **Express**
-   **TypeScript**
-   **TypeORM** Hibernate, Doctrine, Entity Framework ORM
-   **TypeDI** Dependency injection for JavaScript and TypeScript
-   **Routing-Controllers** Structured, declarative, and beautifully configured class-based controller
-   **JWT** AccessToken, RefreshToken
-   **ESLint, Prettier** Maintain code style consistency
-   **Jest** Unit test
-   **SuperTest** E2E

### Environment Variable Management

Remove any `.sample` suffix in the `config/`

```env
PORT=3000
API_PREFIX=api
DATABASE_HOST=127.0.0.1
DATABASE_PORT=3306
DATABASE_USERNAME=development
DATABASE_PASSWORD=development
DATABASE_NAME=development
TYPEORM_SYNCHRONIZE=true
TYPEORM_LOGGING=true
JWT_SECRET_ACCESS_KEY=test!@#$
JWT_SECRET_REFRESH_KEY=retest!@#$
```

### Build Setup

Steps to run this project:

1. Run `yarn install` command
2. Run `yarn start` command

### Database Migration

To run all migrations

```sh
$ yarn run typeorm migration:run
```

To revert last migration

```sh
$ yarn run typeorm migration:revert
```

To create new migration

```
$ yarn run typeorm migration:generate -n <Name of migration class>
```

Check further usages in [typeorm](https://typeorm.io/#/migrations)

### Tests

```sh
# e2e, unit tests
$ yarn test
```

### Troubleshooting

1. `error function uuid_generate_v4() does not exist : QueryFailedError: function uuid_generate_v4() does not exist`
   This error can be fixed by running `CREATE EXTENSION IF NOT EXISTS "uuid-ossp";` inside the Database
2. Make sure you've already create role & database as what's written in the .env file, if it's development then you should create `development` DB & `development` role with `development` as the password
3. `error Cannot read property 'length' of undefined : TypeError: Cannot read property 'length' of undefined`
   If you encounter error to run the service in the local make sure you already have .env file in the folder `/config/`. You should remove the `.sample` extension to use it.
4. If you found any other in the console, you can try to see the verbose stacktrace from `/logs/` directory
