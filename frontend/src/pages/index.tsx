import * as React from 'react';
import { NextPage } from 'next';

import { PageWrapper, Content, Column } from 'components/layout';
import { Stack, Text } from 'components/ui-core';

const Section = Content.withComponent('section');

const IndexPage: NextPage<{}> = () => (
  <PageWrapper>
    <Section>
      <Column>
        <Stack spacing="xxl">
          <Text>Welcome!</Text>
        </Stack>
      </Column>
    </Section>
  </PageWrapper>
);

export default IndexPage;
