/* eslint-disable */
const withSourceMaps = require('@zeit/next-source-maps')();

const API_BASE = 'http://localhost:8000/';

module.exports = withSourceMaps({
  env: {
    BACKEND_API_BASE: process.env.BACKEND_API_BASE || API_BASE,
    SENTRY_DSN: process.env.SENTRY_DSN,
  },
});
